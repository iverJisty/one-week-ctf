require 'digest/md5'

class ProblemsController < ApplicationController

	before_action :check_login_state	


	def index
		if request.method == "POST"
			ans = Flag.find_by_question params[:user][:question]
			if ans.flag == params[:user][:flag]
				flash.now[:success] = "You got the flag!!"
			else
				flash.now[:error] = "T __ T "
			end

		end

		@question_list = Array.new
		Flag.all.each do |q|
			@question_list.push(q.question)
		end
		render :index
	end

	def pwn100
		send_file("#{Rails.root}/public/pwn100.tgz",filename:"pwn100-"+Digest::MD5.hexdigest('pwn100')+".tgz")
	end

	def pwn200
		send_file("#{Rails.root}/public/pwn200.tgz",filename:"pwn200-"+Digest::MD5.hexdigest('pwn200')+".tgz")
	end

	def pwn300
		send_file("#{Rails.root}/public/pwn300.tgz",filename:"pwn300-"+Digest::MD5.hexdigest('pwn300')+".tgz")
	end

	def sqlpwn
		send_file("#{Rails.root}/public/sqlpwn.tgz",filename:"sqlpwn-"+Digest::MD5.hexdigest('sqlpwn')+".tgz")
	end
end
