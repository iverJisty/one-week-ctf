class UserController < ApplicationController
	

	def login
		if request.method == "POST"
			@user = User.find_by_username params[:user][:username]
			if @user.nil? || !(@user.authenticate params[:user][:password])
				flash[:error] = "Username or password incorrect"
				session.delete :username
				redirect_to :login
			else
				flash[:success] = "Login Success!!"
				session[:username] = @user.username
				redirect_to :root
			end
		end
	end

	def logout
		session.clear
		flash[:success] = "Logout Success!!"
		redirect_to :login
	end



end
