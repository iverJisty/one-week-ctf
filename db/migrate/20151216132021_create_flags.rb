class CreateFlags < ActiveRecord::Migration
  def change
    create_table :flags do |t|
	  t.string :question
	  t.string :flag
      t.timestamps null: false
    end
  end
end
